import csv

with open('tls-parameters-4.csv') as csv_file:
    csv_reader = csv.DictReader(csv_file)
    cipher_suites = {}
    for row in csv_reader:
        if row['DTLS-OK'] == 'Y' and row['Recommended'] == 'Y':
            value = row['Value'].replace('0x', '')
            description = row['Description']
            reference = row['Reference']
            cipher_suites[value] = f"{description} ({reference})"
with open('registry-ciphers.py', 'w') as py_file:
    py_file.write('registry_ciphersuites = {\n')
    for value, description in cipher_suites.items():
        py_file.write(f"\t'{value}': '{description}',\n")
    py_file.write('}\n')
