import subprocess
import re

# Set default values for variables
srcdir = '.'
top_builddir = '../..'

# Run GCC and extract relevant lines from output
gcc_output = subprocess.run(['gcc', '-E', f'{srcdir}/../../lib/algorithms/ciphersuites.c', 
                             '-I', top_builddir, '-I', f'{srcdir}/../../lib', '-DHAVE_CONFIG_H', 
                             '-DHAVE_LIBNETTLE', '-I', f'{srcdir}/../../gl', '-I', f'{srcdir}/../includes', 
                             '-DENABLE_DHE', '-DENABLE_ECDHE', '-DENABLE_PSK', '-DENABLE_ANON', '-DENABLE_SRP'],
                            stdout=subprocess.PIPE, text=True)
cs_algorithms = re.search(r'static const gnutls_cipher_suite_entry_st cs_algorithms.*?;', 
                          gcc_output.stdout, flags=re.DOTALL).group(0)

# Extract relevant information about each cipher suite and format into a Python dictionary
cipher_suites = {}
for line in cs_algorithms.splitlines():
    if '{' in line:
        parts = line.strip().split()
        id_num = int(parts[0][:-1])
        gnutlsname = parts[1].rstrip(',')
        cipher = parts[2]
        kx = parts[3]
        mac = parts[4] if parts[5] != 'AEAD' else parts[7]
        min_version = parts[6]
        min_dtls_version = parts[7]
        prf = parts[8] if parts[5] != 'AEAD' else ''
        cipher = cipher.replace('ARCFOUR', 'RC4').replace('3DES-CBC', '3DES-EDE-CBC')
        kx = kx.replace('ANON-', '') + ('-anon' if 'ANON-' in kx else '').replace('SRP', 'SRP-SHA')
        mac = mac.replace('UMAC-', 'UMAC').replace('DIG-', '').replace('SHA1', 'SHA')
        name = f"TLS_{kx}_WITH_{cipher}"
        if parts[5] == 'AEAD' and 'GCM' in cipher:
            name += f"_{mac}"
        else:
            name += f"_{mac}"
        name = name.replace('-', '_')
        cipher_suites[id_num] = {
            'id': id_num,
            'name': name,
            'gnutlsname': gnutlsname,
            'cipher': cipher,
            'kx': kx,
            'mac': mac,
            'min_version': min_version,
            'min_dtls_version': min_dtls_version,
            'prf': prf
        }

# Sort the dictionary by ID and print as a formatted string
sorted_suites = sorted(cipher_suites.items())
print("var gnutls_ciphersuites = {")
for id_num, suite in sorted_suites:
    print(f"  {id_num}: {{ id: {id_num}, name: \"{suite['name']}\", gnutlsname: \"{suite['gnutlsname']}\", "
          f"cipher: \"{suite['cipher']}\", kx: \"{suite['kx']}\", mac: \"{suite['mac']}\", "
          f"min_version: \"{suite['min_version']}\", min_dtls_version: \"{suite['min_dtls_version']}\", "
          f"prf: \"{suite['prf']}\" }
